module.exports = {
     verifyToken(req, res, next){
        const jwt = require('jsonwebtoken') 
        const secret = require('../../token')
        const authHeader = req.headers['authorization'] || null
        const token =  authHeader && authHeader.split(' ')[1]
        if(!authHeader) return res.status(400).send('not found authHeader')
        if(!token) return res.status(400).send('not found token')
    
        jwt.verify(token, secret.ACCESS_TOKEN_SECRET, (err, data)=>{
            if(err) return res.status(400).send(err)
            req.data = data  
            next() 
        })
    }
}
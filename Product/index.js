const express = require('express')
const cors = require('cors')
const pool = require('./db')
const path = require('path')
const verifyToken = require('./middlewares/verifyToken')

const app = express()

app.use(express.static(path.join(__dirname, 'images')))
app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use(cors({orgin:'*',
              methods: ['GET','POST','DELETE','PUT'],
              allowedHeaders:['Content-Type', 'Authorization']}))

const productTemp = require('./controllers/product')
const productController = productTemp(pool) 

const orderTemp = require('./controllers/order')
const orderController = orderTemp(pool)

const historyTemp = require('./controllers/history')
const historyController = historyTemp(pool)

app.get('/product',productController.getProduct)
app.get('/product/:id',productController.getProductById)
app.post('/order', verifyToken.verifyToken, orderController.createOrder)
app.put('/order', verifyToken.verifyToken, orderController.cancelOrder)
app.get('/order', verifyToken.verifyToken, orderController.viewOrder)
app.get('/history', verifyToken.verifyToken, historyController.history)

app.listen(3001)







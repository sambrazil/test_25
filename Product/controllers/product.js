module.exports = function(pool){
 return{
    async getProduct(req,res){
          const [products] = await pool.query('SELECT * FROM product')
          if(!products[0]) return res.status(400).json({error:'not found product'})
          res.json(products)
    },
    async getProductById(req,res){
      const id = req.params.id    
      const [products] = await pool.query('SELECT * FROM product WHERE id =?',[id])
      if(!products[0]) return res.status(400).json({error:'not found product'})
      res.json(products)
}

 }
}
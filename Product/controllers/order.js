module.exports = function(pool){
    return{
      async createOrder(req, res){
        const {user_id, firstname, lastname, address, telephone} = req.data     
          
         const {product_id, product_name, product_picture, product_price, product_amount, logistic_name,
               logistic_price } = req.body || null
          

         if(user_id ==='' || product_id ==='' || product_name ==='' || product_picture ==='' || product_price ==='' ||
          product_amount ==='' || logistic_name ==='' ||	logistic_price ==='' || firstname ==='' || lastname ==='' || 
          address ==='' || telephone ==='') 
          return res.status(400).json({error:"please input all data"})

         if(product_amount <= 0) return res.status(400).json({error:"can not order with amount <= 0"})

         const [checkStock] = await pool.query('SELECT amount FROM product WHERE id=?',[product_id])
           if(!checkStock[0]) return res.status(400).json({error:'not found product'})

           const checkProductAmount = checkStock[0].amount
           if(checkProductAmount <= 0) return res.status(400).json({error:'this prouct out of stock'})
           if(checkProductAmount < product_amount) return res.status(400).json({error:'can not order more than in stock'})

         const restProductAmount = checkProductAmount - product_amount
         const [updateStock] = await pool.query(`UPDATE product SET amount = ? WHERE id = ?`,[restProductAmount, product_id]) 
                           
          const [createOrder] = await pool.query(`INSERT INTO orders 
                                                (user_id, status, product_id, product_name, product_picture, product_price, 
                                              product_amount, logistic_name,	logistic_price, firstname, lastname, 
                                                address, telephone )
                                                VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`
                                                ,[user_id, "packing", product_id, product_name, product_picture, product_price, 
                                                product_amount, logistic_name,	logistic_price, firstname, lastname, address, 
                                                telephone ])

         
          res.json({status: 'create order success'})                                      
        },
       async cancelOrder(req, res){
            const {user_id} = req.data
            const {id, product_id} = req.body

            const[checkOrder] = await pool.query('SELECT product_amount, status FROM orders WHERE id=? AND user_id=?',[id,user_id])
            if(!checkOrder[0]) return res.status(400).json({error:'not found order'})
            if(checkOrder[0].status === 'cancel') return res.status(400).json({error:"already canceled"})

            const [checkStock] = await pool.query('SELECT amount FROM product WHERE id=?',[product_id])
            if(!checkStock[0]) return res.status(400).json({error:'not found product'})

            const restProductAmount = checkStock[0].amount + checkOrder[0].product_amount 
            const [updateStock] = await pool.query('UPDATE product SET amount = ? WHERE id=?',[restProductAmount, product_id])

            const[cancelOrer] = await pool.query('UPDATE orders set status =? WHERE id=? AND user_id=?',['cancel',id, user_id])
            res.json({status:"cancel order success"})
        },
       async viewOrder(req, res){
            const {user_id} = req.data
            const [viewOrder] = await pool.query('SELECT * FROM orders WHERE user_id =? AND status NOT IN (?) ORDER BY id DESC',[user_id,'cancel'])
            if(!viewOrder[0]) return res.json({status:"not found order"})
            res.json(viewOrder)
        }
    }
  }
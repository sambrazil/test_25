module.exports = function(pool){
    return{
       async history(req, res){
        const {user_id} = req.data
        const [viewOrder] = await pool.query('SELECT * FROM orders WHERE user_id =?  ORDER BY id DESC',[user_id])
        if(!viewOrder[0]) return res.json({status:"not found order"})
        res.json(viewOrder)
        }
    }
}
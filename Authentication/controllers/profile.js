module.exports = function(){
    return{
        getProfile(req, res){
          const  {user_id, email, firstname, lastname, address, telephone} = req.data
          res.json({
               user_id: user_id,
                email: email,
                firstname: firstname,
                lastname: lastname,
                address: address,
                telephone: telephone
              
          })
        }
    }
}
module.exports = function(pool){
    return{
         async postLogin (req, res)  {
            const jwt = require('jsonwebtoken')
            const bcrypt = require('bcrypt')
            const token = require('../../token')
            const {email, password} = req.body || null 
            if(email ==='') res.status(400).json({error:'please input email'})
            if(password === '') res.status(400).json({error:'please input password'})
        
            const [checkEmail] = await pool.query('SELECT * FROM user WHERE email =?',[email])
            if(!checkEmail[0]) return res.status(400).json({error: 'not found email'})
            
            const comparePassword = await bcrypt.compare(password, checkEmail[0].password)
            if(!comparePassword)  return res.status(400).json({error: 'worng password'})
            const payload = { user_id: checkEmail[0].id,
                             email: checkEmail[0].email,
                             firstname: checkEmail[0].firstname,
                             lastname: checkEmail[0].lastname,
                             address: checkEmail[0].address,
                             telephone: checkEmail[0].telephone
                           }
           const accessToken = await jwt.sign(payload, token.ACCESS_TOKEN_SECRET, {expiresIn:60*500})
           const refreshToken = await jwt.sign(payload, token.REFRESH_TOKEN_SECRET)
           const [checkRefreshToken] = await pool.query('SELECT * FROM token WHERE user_id =?',payload.user_id)
           if(!checkRefreshToken[0]){
               const [insertRefreshToken] = await pool.query(` INSERT INTO token (refresh_token, user_id) VALUES (?,?)`
                                                               ,[refreshToken, payload.user_id])
           } 
           else{
              const [updateRefreshToken] = await pool.query(` UPDATE token SET refresh_token =? WHERE user_id =? `
                                                              ,[refreshToken, payload.user_id])
           }
           res.json({ accessToken: accessToken,
                      refreshToken: refreshToken
                   })
        
        }
    }
}
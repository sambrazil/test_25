module.exports = function(pool){
    return{
        async postRegister(req, res){
            const bcrypt = require('bcrypt')
            const {email, password, confirmPassword,firstname, lastname ,address ,telephone} = req.body || null
            if( email === '' || password === '' || confirmPassword === ''|| firstname === '' 
              || lastname === '' || address === '' || telephone === '') return res.status(400).json({error: 'please input all data'})
            const[checkEmail] = await pool.query('SELECT email FROM user WHERE email =?',[email])
            if(checkEmail[0]) return res.status(400).json({error: 'email already in use'})
        
            if(password !== confirmPassword) return res.status(400).json({error: 'password not match'})
            bcrypt.hash(password, 10, async function(err, hash) {
              // Store hash in your password DB.
              const [rows] = await pool.query(` INSERT INTO user (email, password, firstname, lastname ,address ,telephone)
              VALUES (?,?,?,?,?,?)`,[email, hash, firstname, lastname ,address ,telephone])
        res.json({status: 'register complete'})   
          });
                                       
        }
    }
}
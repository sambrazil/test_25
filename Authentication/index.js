const express = require('express');
const pool = require('./db')
const cors = require('cors')
const verifyToken = require('./middlewares/verifyToken')

const app = express()
app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use(cors({orgin:'*',
              methods: ['GET','POST'],
              allowedHeaders:['Content-Type', 'Authorization']}))

const registerTemp = require('./controllers/register')
const registerController = registerTemp(pool)

const loginTemp = require('./controllers/login')
const loginController = loginTemp(pool)

const profileTemp = require('./controllers/profile')
const profileController = profileTemp()

app.post('/register',registerController.postRegister)
app.post('/login', loginController.postLogin)
app.get('/profile',verifyToken.verifyToken,profileController.getProfile)

app.listen(3000)